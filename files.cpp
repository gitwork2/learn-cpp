#include <iostream>
#include <fstream>
using namespace std;

int main() 
{
	fstream file; // fstream object
	
	/*
	file.open("text.txt", ios::out); // open file with write
	file.open("text.txt", ios::in);  // open file with read
	file.open("text.txt", ios::out | ios::in);  // open file with read

	file.open("file"); // open file
	file.close();    // close file
	file << "hello"; // write to file
 	*/

 	
	// =========================== WRITE TO FILE ============================
	file.open("text.txt", ios::out); // open file with write
	
	if (!file) {
		cout << "File not created!";
	}
	else {
		cout << "File created successfully!";
		file << "hello"; // write to file
		file.close(); 	 // close file 
	}

	// =========================== READ FROM FILE ============================
	file.open("my_file.txt", ios::in); // open file with read
	
	if (!file) {
		cout << "No such file";
	} else {
		char ch;

		while (true) {
			file >> ch;
			if (file.eof())
				break;

			cout << ch;
		}
	
	}
	file.close(); // close file

	return 0;	
}
