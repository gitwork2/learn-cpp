// Пузырковая сортировка
#include <iostream>
#include <algorithm> // для std::swap

int main() {
	using namespace std;
	const short lenght = 5;
	int arrey[lenght] = {3, 1, 2, 4, 5};
	
	for (int indx = 0; indx < lenght - 1; ++indx) {
		int smallestIndx = indx;
		for (int currentIndx = indx + 1; currentIndx < lenght; ++currentIndx) {
			if (arrey[currentIndx] < arrey[smallestIndx]) {
				smallestIndx = currentIndx;
			}
		}
		swap(arrey[indx], arrey[smallestIndx]);
	}
	for (int i = 0; i < lenght; ++i) {
		cout << arrey[i] << " ";
	}
	cout << endl;

	return 0;
}
