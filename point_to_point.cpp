#include <iostream>
using namespace std;

int main()
{
	int a = 23;
	int *ptr = &a; // Normal point

	int **ptr_ptr = &ptr;      // Mega point
	cout << **ptr_ptr << endl; // Разыменование указателя на указатель

	// Динамическое ыделение массива указателей
	int **arr_ptr = new int*[20];
	
	// Динамическое выделение двумерного массива
	// В случае если известно количество элементов в строке compile-time
	int (*array)[5] = new int[20][5];
	auto b_array = new int[15][10];

	// В случае динамического выделения и строк и рядов
	int **full_arr = new int*[row];
	for (int i < col; i < col; ++i)
		full_arr[i] = new int*[col];

	// Удаление такого масиива
	for (int i = 0; i < col; ++i)
		delete[] full_arr[i];
	delete[] full_arr; 

	// А ЕЩЁ МОЖНО СДЕЛАТЬ ТАК
	float ***ptr_three;
	float ****ptr_four;
	
	return 0;
}
