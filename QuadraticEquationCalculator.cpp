#include <iostream>
#include <cmath>

int main() {
	std::cout << "ax^2 + bx + c = 0" << '\n';
	double a;
	double b;
	double c;
	double x;
	double d;
	double x1;
	double x2;

	std::cout << "a = ";
	std::cin >> a;

	std::cout << "b = ";
	std::cin >> b;

	std::cout << "c = ";
	std::cin >> c;
	
	std::cout << "x = ";
	std::cin >> x;

	d = pow(b, 2) - (4*a*c);
	std::cout << "D = 4 - 4ac = " << d << '\n';

	if (d > 0) {
		x1 = (-b + sqrt(d)) / (2*a);
		std::cout << "x1 = -b + sqrt(D) / 2a = " << x1 << '\n';

		x2 = (-b - sqrt(d)) / (2*a);
		std::cout << "x2 = -b - sqrt(D) / 2a = " << x2 << '\n';  
	}

	if (d == 0) {
		x1 = -b / (2*a);
		std::cout << "x1, x2 = -b +- 0 / 2a = " << x1 << '\n';
	}

	if (d < 0) {
		std::cout << "No roots!\n";		
	}
	return 0;
}
