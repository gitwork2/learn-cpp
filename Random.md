## Generator random numbers

Right generator

	#include <iostream>
	#include <cstdlib> // для функций rand() и srand()
	#include <ctime>   // для функции time()

	// Генерируем рандомное число между значениями min и max.
	// Предполагается, что функцию srand() уже вызывали
	int getRandomNumber(int min, int max)
	{
		static const double fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);

		// Равномерно распределяем рандомное число в нашем диапазоне
		return static_cast<int>(rand() * fraction * (max - min + 1) + min);
	}

	int main()
	{
		srand(static_cast<unsigned int>(time(0))); // устанавливаем значение системных часов в качестве стартового числа
		
	}
