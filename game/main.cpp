#include <keyboard.h> // for get keyboard keys (getch())
#include <iostream>

const short width  = 98;
const short heigth = 36;
static bool map_arr[heigth][width] = { };
static short x = 0;
static short y = 0;

void map_print() {
	map_arr[heigth/2][width/2]     = true;
	map_arr[heigth/2][width/2+1]     = true;
	map_arr[heigth/2+1][width/2]     = true;
	map_arr[heigth/2+1][width/2+1]     = true;
	map_arr[23][59] = true;
	map_arr[24][59] = true;
	map_arr[25][59] = true;
	map_arr[26][62] = true;
	map_arr[27][63] = true;
	map_arr[28][64] = true;
	map_arr[29][65] = true;

	for (int i = 0; i < heigth; ++i) {
		for (int j = 0; j < width; ++j) {
			if (map_arr[i][j] == true) {
				printf("\033[%d;%dH", i+1, j+1);
				std::cout << "█";
			}
		}
		std::cout << '\n';
	}
}

int main() {
	using namespace std;
	system("clear");
	while (true) {
		printf("\033[%d;%dH", 0, 0);
		cout << "PosX: " << x << '\n';
		cout << "PosY: " << y << '\n';
		map_print();

		switch (getch()) {
			case 'w':
				--y;
				if (map_arr[y][x] == true) ++y;
		 		break;
			case 's':
				++y;
				if (map_arr[y][x] == true) --y;
				break;
			case 'a':
				--x;
				if (map_arr[y][x] == true) ++x;
				break;
			case 'd':
				++x;
				if (map_arr[y][x] == true) --x;
				break;
			default:
				std::cout << "LOL\n";
			}
		if (x > width) --x;
		if (x < 0)  ++x;
		if (y > heigth) --y;
		if (y < 0)  ++y;
		printf("\033[%d;%dH", y+1, x+1);
		cout << "#";
		system("clear");	
	}
	return 0;
}
