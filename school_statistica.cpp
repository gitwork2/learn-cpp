// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// Grades status at school
/*
Программа статистики по шк. предметам
Можно оценить только по 7 оценкам
V1.2
*/
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include <iostream>
#include <algorithm>
#include <cmath>

std::string school_item[] {
	"Math     ",
	"Physics  ",
	"English  ",
	"Sociology"
};

int grades[][8] = {
	{5, 5, 4, 5, 5, 5, 5}, // math
	{5, 3, 5, 5, 5, 5, 5}, // phisics
	{5, 5, 5, 4, 3, 4, 4}, // english
	{5, 4, 5, 4, 5, 5, 5}  // siciology
};
double score[] = {0};

int main() {
	using namespace std;
	double perc = 0;
	short result_perc[4] = {0};

	// среднее орифметическое и получение балла в процентах
	for (int line = 0; line < 4; ++line) {
		for (int j = 0; j < 8; ++j) {
			score[line] += static_cast<double>(grades[line][j]);
			perc = round(score[line]/7*100)/100;
			result_perc[line] = round(perc * 100 / 5);
		}
	}

	// sort array result percent
	for (int indx = 0; indx < 4; ++indx) {
		int smallestIndx = indx;
		for (int currentIndx = indx + 1; currentIndx < 4; ++currentIndx) {
			if (result_perc[currentIndx] > result_perc[smallestIndx]) {
				smallestIndx = currentIndx;
			}
		}
		// replace index
		swap(result_perc[indx], result_perc[smallestIndx]);
		swap(school_item[indx], school_item[smallestIndx]);
		swap(score[indx], score[smallestIndx]);
	}
	
	for (int i = 0; i < static_cast<int>(sizeof(school_item) / sizeof(school_item[0])); ++i) {
		cout << school_item[i] << " = " << round(score[i]/7*100)/100 << " = " << result_perc[i] << "%"<< endl; // шк. предметы и их статистика
		// визуализация балла 
		for (int perc_print = 0; perc_print < result_perc[i]; ++perc_print) {
			cout << "#";
		}
		// сколько не хватает до 100 баллов
		for (int perc_end = 0; perc_end < 100 - result_perc[i]; ++perc_end) {
			cout << ".";
		}
		cout << endl;
	}

	return 0;
}

/*
Physics   = 5 = 100%
####################################################################################################
Math      = 4.86 = 97%
#################################################################################################...
Sociology = 4.71 = 94%
##############################################################################################......
English   = 4.29 = 86%
######################################################################################..............
*/

// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// End the program
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
