#include <iostream>
using namespace std;
using arithmeticFnc = double(*)(double, double);

double add(double a, double b)
{
	return a + b;
}

double subtract(double a, double b)
{
	return a - b;
}

double multiply(double a, double b)
{
	return a * b;
}

double divide(double a, double b)
{
	return a / b;
}

struct arithmeticStruct {
	char opr;
	arithmeticFnc fncPtr;
};
static arithmeticStruct arithmeticArray[] {
	{'+', add},
	{'-', subtract},
	{'*', multiply},
	{'/', divide}
};

arithmeticFnc getArithmeticFnc(char opr) 
{
	for (auto &element : arithmeticArray) {
		if (opr == element.opr) {
			return element.fncPtr;
		}	
	}
}

int main()
{
	char opr;
	while (true) { // better do while
		cout << "Enter + - / * : ";
		cin >> opr;
		if (opr == '+' || opr == '-' || opr == '*' || opr == '/') break;
	}
	double a;
	double b;
	cout << "a = "; cin >> a;
	cout << "b = "; cin >> b; 

	arithmeticFnc fncResult = getArithmeticFnc(opr);
	cout << fncResult(a, b) << endl;
	return 0;
}
