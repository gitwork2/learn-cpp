// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// Write event with system time
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include <iostream>// for print output
#include <chrono>  // for system time
#include <fstream> // for files
using namespace std;

string system_clock() {
	time_t end_time = chrono::system_clock::to_time_t(chrono::system_clock::now()); 
	return ctime(&end_time);
}

int main() {
	fstream file; // fstream object
	string event; // user's event

	// for user
	cout << "Hello, that program is a time black.\n";	
	cout << "You can write your events and they are saving with system time.\n";
	cout << "All result save in a file 'result.txt' in project dir.\n";
	cout << "Enter 'program quit' if you want quit.\n";



	file.open("result.txt", ios::out); // open file with write
		
	if (!file) { cout << "Error created file!\n"; }
	else {
	while (true) {
		cout << ">> ";
		getline(cin >> ws, event); // get user's message
		file << system_clock() << event << endl << "" << endl; // write to file
		if (event == "program quit") { break; }
		event = ""; // clear var	
	}
		file.close(); // close file 
	}
	
	return 0;
}

// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// End the program
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
