// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// Data list
// Программа позволяет создовать пронумированные списки,
// и сохроняет этот список в файл list.txt.
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include <iostream>
#include <fstream>
using namespace std;

int main() {
	fstream file; // fstream object

	cout << "Hello, введи общее кольчество элементов списка: ";
	int list_size;
	cin >> list_size;
	string list[list_size];
	
	cout << "Введи 'system_replase' - если хочешь сменить размер списка\n";
	cout << "Ok, теперь можешь заполнить список,\n" 
	<< "введи 'system_exit' если хочещь выйты из режима заполнения\n";

	// =========================== WRITE TO FILE ============================
	file.open("list.txt", ios::out); // open file with write
		
	if (!file) {
		cout << "Error!\n"; // если файл не сгенерируется, то выведется сообщение об ошибке
	}
	else {
		// заполняем список
		for (int k = 0; k < list_size; ++k) {
			cout << k << ". >> ";
			getline(cin >> ws, list[k]);
			file << k << ". " <<  list[k] << endl; // write to file

			// если человек захочет сменить размер списка
			if (list[k] == "system_replase") {
				cout << "New size: ";
				cin >> list_size;
			}
			// если человек захочет выйти, не заполнив список
			if (list[k] == "system_exit") {
				break;
			}
		}
		file.close(); // close file 
	}
	return 0;
}

// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// End the program
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
