# Invalid input

## №1 Извлечение из cin успешно, но есть лишнии данные

	Enter number: 7hsfd // invalid input

	// Решение
	cout << "Enter number: ";
	cin >> var;
	cin.ignore(32767, '\n');

## №2 Извлечение невыполнено

	Enter number: a

	// Решение
	while (true) {
	cout << "Enter number: "; 
	// Enter number: f
	cin >> var;
	
	if (std::cin.fail()) { // если предыдущее извлечение было неудачным,
		std::cin.clear();  // то возвращаем cin в 'обычный' режим работы
		std::cin.ignore(32767,'\n'); // и удаляем значения предыдущего ввода извходного буфера
	} else {
		break;
	}
	}
	
## №3 Пользователь ввел слишком большое число

	Решение такое же, как и в номере 2
