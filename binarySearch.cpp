#include <iostream>
#include <algorithm>

int main() {
	int arr[100];

	int left = 0;
	int right = sizeof(arr)/sizeof(arr[0]);
	int mid;
	int current;

	for (int i = 0; i < right; ++i) {
		arr[i] = rand()%300;	
	}

	std::sort(std::begin(arr), std::end(arr));
	for (int i = 0; i < right; ++i) {
		std::cout << arr[i] << '\t' << i << '\n';
	}
	
	int usrInp;
	std::cout << "Num: "; std::cin >> usrInp;

	while (left != right) { // проходим по всему массиву
		mid = (left+right) / 2; // индекс серидины
		current = arr[mid];     // значение серидины в массиве

		if (current == usrInp) { // ЕСЛИ НАШЛИ НУЖНОЕ ЧИСЛО В МАССИВЕ
			std::cout << "Num =  " << current << '\n';
			std::cout << "Indx = " << mid << '\n';
			break;
		} 

		if (current < usrInp) {
			++left; // увеличиваем mid и смещаемся вправо
		}
		else {
			--right; // уменьшаем mid и смещаемся влево
		}

		std::cout << "Num wasn't found!\n";
	}
	
	return 0;
}
