#include <iostream>

int main() {
	double a, b, h, s;
	int usrChoose;

	enum Shape {
		RECTANGLE,
		SQUARE,
		PARALLELOGRAM,
		TRIANGLE,
		RHOMBUS,
		TRAPEZOID
	};
	
	std::cout << "Square of:\n"
	<< "1. Rectangle\n"
	<< "2. Square\n"
	<< "3. Parallelogram\n"
	<< "4. Triangle\n"
	<< "5. Rhombus\n"
	<< "6. Trapezoid\n";
	
	std:: cin >> usrChoose;
	 
	switch (--usrChoose) {
		case RECTANGLE:
			std::cout << "a = "; std::cin >> a;
			std::cout << "b = "; std::cin >> b;
			
			s = a*b;
			std::cout << "S = ab = " << s << '\n';
			break;
		case SQUARE:
			std::cout << "a = "; std::cin >> a;
			
			s = a*a;
			std::cout << "S = a^2 = " << s << '\n';
			break;
		case PARALLELOGRAM:
			std::cout << "a = "; std::cin >> a;
			std::cout << "h = "; std::cin >> h;

			s = a*h;
			std::cout << "S = ah = " << s << '\n';
			break;
		case TRIANGLE:
			std::cout << "a = "; std::cin >> a;
			std::cout << "h = "; std::cin >> h;

			s = (a*h)*0.5;
			std::cout << "S = ah / 2 = " << s << '\n';
			break;
		case RHOMBUS:
			std::cout << "d1 = "; std::cin >> a;
			std::cout << "d2 = "; std::cin >> b;

			s = (a*b)*0.5;
			std::cout << "S = d1*d2 / 2 = " << s << '\n';
			break;
		case TRAPEZOID:
			std::cout << "a = "; std::cin >> a;
			std::cout << "b = "; std::cin >> b;
			std::cout << "h = "; std::cin >> h;

			s = (a+b) / 2 * h;
			std::cout << "S = (a+b) / 2 * h = " << s << '\n';
			break;
		default:
			std::cout << "WTF?!\n";
			break;
	}
	return 0;
}
