## Cycle do while

	short i = 0;
	do {
		cout << "Enter 4 or 7: ";
		cin >> i;		
	} while (i != 4 && i != 7);
	/*
	Result:
	Enter 4 or 7: 9
	Enter 4 or 7: 7
	(exit)
	*/

If i = 4 or 7 we exit, else we ask again!

	// ===================================================

	char i;
		do {
			cout << "Hello\n";
			do {
				cout << "Exit[y/n]: ";
				cin >> i;	
			} while (i != 'y' && i != 'n');
		} while (i != 'y');
	/*s
	Result:	
	Hello
	Exit[y/n]: n
	Hello
	Exit[y/n]: y
	(exit)
	*/
	
If enter 'y' exit, but if enter 'n' print "hello".
