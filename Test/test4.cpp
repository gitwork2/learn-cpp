// Test 4 (structurs)
#include <iostream>
using namespace std;

struct monstr {
    string name;
    int hp = 0;
};

void printMonster(monstr orc, monstr goblin) {
    cout << "Monster orc name's " << orc.name << " has " << orc.hp << " hp\n";
    cout << "Monster goblin name's " << goblin.name << " has " << goblin.hp << " hp\n";
}

int main() {
    monstr orc = {"Killer", 90};
    monstr goblin = {"Empty", 150};
    printMonster(orc, goblin);
    return 0;
}