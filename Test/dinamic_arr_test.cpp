// Sort I brought from teach book
#include <iostream>
using namespace std;

void sortArray(string *array, int length) {
	for (int indx = 0; indx < length; ++indx) {
		int smallest_indx = indx;
		for (int smallest_item = indx+1; smallest_item < length; ++smallest_item) {
			if (array[smallest_item] < array[smallest_indx]) {
				smallest_indx = smallest_item;
			}
		}
		swap(array[indx], array[smallest_indx]);
	}
}

int main() {
	cout << "Hello, enter how many names you would like to enter: ";
	int length = 0;
	cin >> length;
	string *arr_names = new string[length];
	for (int indx = 0; indx < length; ++indx) {
		cout << "Enter name #" << indx << ": ";
		getline(cin >> ws, arr_names[indx]);
	}
	sortArray(arr_names, length);
	for (int k = 0; k < length; ++k) {
		cout << "Name #" << k << ": " << arr_names[k] << '\n';
	}
	return 0;
}
