#include <iostream>
using namespace std;

void replace(int &num1, int &num2)
{
	int rnum1 = num1;
	num1 = num2;
	num2 = rnum1;
}

int main()
{
	int a = 5;
	int b = 7;
	cout << "a = " << a << " b = " << b << endl;
	replace(a, b);
	cout << "a = " << a << " b = " << b << endl;	
	return 0;
}
