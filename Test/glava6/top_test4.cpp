#include <iostream>
using namespace std;

void print(char *str)
{
	while (true) {
		if (*str == '\0') break;
		cout << *str;
		++str;
	}
}

int main()
{
	char ptrStr[] {"Hello, World!"};
	print(ptrStr);
	return 0;
}
