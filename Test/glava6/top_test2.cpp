#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
	struct Student {
		string name;
		int score;	
	};
	int length;

	cout << "How many students? ";
	cin >> length;

	Student *list = new Student[length+1];
	for (int i = 0; i < length; ++i) {
		// Get name
		cout << "\nName  student's #" << i << ": ";
		getline(cin >> ws, list[i].name);
		// Get score
		cout << "Score student's #" << i << ": ";
		cin >> list[i].score;
	}

	// Sort
	for (int i = 0; i < length-1; ++i) {
		for (int k = 0; k < length; ++k) {
			if (list[k].score < list[k+1].score) {
				swap(list[i], list[i+1]);
			}			
		} 

	}
	// Print
	for (int indx = 0; indx < length; ++indx) {
		cout << list[indx].name << ' ' << list[indx].score << '\n';
	}
	
	return 0;
}
