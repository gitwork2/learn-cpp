// Poker	
#include <iostream>
#include <array>
using namespace std;

enum class Suid {
	TREFU,
	BUBNU,
	CHERVU,
	PIKI,
	MAX	
};

enum class Rank {
	R2,
	R3,	
	R4,	
	R5,	
	R6,	
	R7,	
	R8,	
	R9,	
	R10,
	VALET,
	DAMA,
	KOROL,
	TYZ,
	MAX
};

struct Card {
	Suid card_suid;
	Rank card_rank;
};
	
void printCard(const Card &item)
{
	switch (item.card_suid) {
		case Suid::TREFU:  cout << 'T'; break;
		case Suid::BUBNU:  cout << 'B'; break;
		case Suid::CHERVU: cout << 'C'; break;
		case Suid::PIKI:   cout << 'P'; break;
		default:
			break;
	}
	switch (item.card_rank) {
		case Rank::R2: cout << '2'; break;
		case Rank::R3: cout << '3'; break;
		case Rank::R4: cout << '4'; break;
		case Rank::R5: cout << '5'; break;
		case Rank::R6: cout << '6'; break;
		case Rank::R7: cout << '7'; break;
		case Rank::R8: cout << '8'; break;
		case Rank::R9: cout << '9'; break;
		case Rank::R10:cout <<"10"; break;
		case Rank::VALET: cout << 'V'; break;
		case Rank::DAMA:  cout << 'D'; break;
		case Rank::KOROL: cout << 'K'; break;
		case Rank::TYZ:   cout << 'T'; break;
		default:
			break;
	}
}

void printDeck(const array<Card, 52> &arrItem)
{
	for (const auto &i : arrItem) {
		printCard(i);
		cout << '\n';
	}
}

void swapCard(Card &struct_a, Card &struct_b)
{
	Card old_a = struct_a;
	struct_a = struct_b;
	struct_b = old_a;
}

void shuffleDeck(array<Card, 52> &arrDeck)
{
	int randIndx;
	for (int current = 0; current < 52; ++current) {
		randIndx = rand() % 52;
		swapCard(arrDeck[current], arrDeck[randIndx]);
	}
}

int getCardInfo(const Card &card)
{
	switch (card.card_rank) {
		case Rank::R2: return 2;
		case Rank::R3: return 3;
		case Rank::R4: return 4;
		case Rank::R5: return 5;
		case Rank::R6: return 6;
		case Rank::R7: return 7;
		case Rank::R8: return 8;
		case Rank::R9: return 9;
		case Rank::R10:   return 10;
		case Rank::VALET: return 10;
		case Rank::DAMA:  return 10;
		case Rank::KOROL: return 10;
		case Rank::TYZ:   return 11;
		default:
			cout << "Aaaaaaaa\n";
			break;
	}	
}
int main ()
{
	srand(time(0));
	array<Card, 52> deck;
	int cardNum = 0;
	for (int i = 0; i < static_cast<int>(Suid::MAX); ++i) {
		for (int k = 0; k < static_cast<int>(Rank::MAX); ++k) {
			deck[cardNum].card_suid = static_cast<Suid>(i);
			deck[cardNum].card_rank = static_cast<Rank>(k);
			++cardNum;
		}
	}
	printDeck(deck);
	shuffleDeck(deck);
	cout << endl;
	printDeck(deck);
	cout << getCardInfo(deck[4]) << endl;
	return 0;
}
