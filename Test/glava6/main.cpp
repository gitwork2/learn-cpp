#include <iostream>
#include <algorithm>
using namespace std;

int main ()
{
	cout << "How many names? ";
	int length;
	cin >> length;
	string *list = new string[length];

	for (int indx = 0; indx < length; ++indx) {
		cout << "Name number " << indx << ": ";
		getline(cin >> ws, list[indx]);
	}
	
	for (int i = 0; i < length-1; ++i) {
		if (list[i] < list[i+1]) {
			swap(list[i], list[i+1]);
		}
	}
	cout << endl;
	for (int indx = length; indx > 0; --indx) {
		cout << "Name number " << list[indx-1] << endl;
	}
	return 0;
}
