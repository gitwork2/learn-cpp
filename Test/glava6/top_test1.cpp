// Good
#include <iostream>
using namespace std;

int totalItems(int *arr, int length)
{
	int total;
	for (int i = 0; i < length; ++i) {
		total += arr[i];
	}
	return total;	
}

enum Items {
	HEALTHER_WATER,
	FIRE_STICK,
	BULLET,	
};

int main() 
{
	int stuff[3];
	stuff[HEALTHER_WATER] = 5;
	stuff[FIRE_STICK] = 3;
	stuff[BULLET] = 50;

	cout << totalItems(stuff, sizeof(stuff)/sizeof(stuff[0])) << '\n';
	return 0;
}
