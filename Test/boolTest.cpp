#include <iostream>
using namespace std;

bool checkDigit(short i) {
	bool g = false;
	if (i == 2)
		g = true;
	else if (i == 3)
	    g = true;
	else if (i == 5)
		g = true;
	else if (i == 7)
		g = true;
	else 
		g = false;
	return (g);
}

int main() 
{
	static short x = 0;
	cout << "Введи простое целое число меньше 10: "; cin >> x;
	if (checkDigit(x)) 
		cout << "The digit is prime" << '\n';
	else 
	    cout << "The digit isn't prime" << '\n';
	if (x < 0)
	    cout << "Error!" << '\n';
	return 0; 
}
