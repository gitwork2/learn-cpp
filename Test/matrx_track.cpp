#include <iostream>
using namespace std;

int main()
{
	srand (time(0));
	int row = 0;
	int col = 0;

	cout << "Row: "; cin >> row;
	cout << "Col: "; cin >> col;
	
	// Выделяем 2-х мерный массив
	int **matrx = new int*[row];
	for (int i = 0; i < row; ++i) 
		matrx[i] = new int[col]; 

	// Заполняем матрицу случайными значениями
	for (int y = 0; y < row; ++y) {
		for (int x = 0; x < col; ++x) {
			matrx[y][x] = rand() % 99;
			cout << matrx[y][x] << ' ';
		}
		cout << endl;
	}

	// Ищем сумму значений по главной диагонали
	int oldX = 0;
	int result;
	for (int y = 0; y < row; ++y) {
		result += matrx[y][oldX];
		++oldX;
	}

	cout << "След матрици = " << result << endl;

	// Удаляем масив
	for (int i = 0; i < col; ++i) 
		delete[] matrx[i];
	delete[] matrx;
	
	return 0;
}
