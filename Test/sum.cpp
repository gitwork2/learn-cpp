#include <iostream>
using namespace std;

int main()
{
	int max = 0;
	cout << "Max:";
	cin >> max;

	int result;
	int num = 0;
	while (num <= max) {
		if (!(num % 5)) {
			result += num;
		}
	++num;
	}
	cout << "Res: " << result << endl;
	return 0;
}
