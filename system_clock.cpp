// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// Test program
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include <iostream>
#include <chrono>

std::string system_clock() {
	std::time_t end_time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()); 
	return std::ctime(&end_time);
}

int main()
{
	std::cout << "System clock: " << system_clock() << std::endl;

    return 0;
}


// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// End the program
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
