#include<iostream>
using namespace std;
char oper = 'x'; // operator +/-  * or / 

int main() {
	double f_num; // first number
	double s_num; // second number

	while (true) {
		cout << "First number: "; cin >> f_num;
		cout << "Second number: "; cin >> s_num;
		cout << "Choose the operator +/-  * or /" << '\n'; cin >> oper;

		if (oper == '-') {
		  cout << "Result: " << f_num << " - " << s_num << " = " << f_num - s_num << endl;
		}
		if (oper == '+') {
		  cout << "Result " << f_num << " + " << s_num << " = " << f_num + s_num << endl;
 	    } 
		if (oper == '/') {
		  cout << "Result " << f_num << " / " << s_num << " = " << f_num / s_num << endl;
		} 
		if (oper == '*') {
		  cout << "Result " << f_num << " * " << s_num << " = " << f_num * s_num << endl;
 	   }
	cout << endl;
	}
    return (0);
}
