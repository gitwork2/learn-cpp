#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

void readFile(vector <string> &text)
{
	fstream file("text.txt", ios::in);
	string str;

	while (!file.eof()) {
		getline(file, str);
		text.push_back(str);
	}
}

int main()
{
	vector <string> text;
	readFile(text);

	char abcAlf[100] {};
	for (short i = 32; i < 127; ++i) {
		abcAlf[i-32] = char(i);
	}
	
	long score[100] {};
	for (int y = 0; y < text.size(); ++y) {
		for (int x = 0; x < text[y].length(); ++x) {
			for (short a = 0; a < 100; ++a) {
				if (text[y].at(x) == abcAlf[a]) ++score[a];
			}
		}
	}

	for (int i = 0; i < 95; ++i) {
		cout << score[i] << " - " << abcAlf[i] << endl;
	}
}
