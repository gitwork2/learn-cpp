#include <iostream>
#include <ctime>
#include <stdlib.h>
using namespace std;

int getRandNum(int min, int max);

int main()
{
	srand(static_cast<unsigned int>(time(0))); // устанавливаем значение

	int biggestNum = 0;
	int arr[10];

	biggestNum = arr[0];
	for (int x = 0; x < 10; ++x)
	{
		arr[x] = getRandNum(0, 23); // заполняем массив рандомными числами от 0 до 23 
		cout << arr[x] << '\n';     // выводим содержание массива
		if (biggestNum < arr[x]) biggestNum = arr[x]; // ищем наибольшее число и присваиваем его пер. biggestNum
	}
	
	cout << "Biggest number in arrey is: " << biggestNum << endl;

	return 0;
}

// Функция рандомных чисел от мин. до макс. (функция не моя я ее взял из примеров =)
int getRandNum(int min, int max)
{
	static const double fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);
	// Равномерно распределяем рандомное число в нашем диапазоне
	return static_cast<int>(rand() * fraction * (max - min + 1) + min);
}
