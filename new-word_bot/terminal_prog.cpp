// Логика программы для заучивания английских слов
#include <iostream>
#include <vector>
using namespace std;

int main() {
	vector<string> words;	 // new words
	vector<string> trans;	 // translate of new words
	vector<string> un_word;  // unknow words
	vector<string> un_trans; // translate of unknow word
	string command;
	string userInp;
	
	while (true) {
		cout << ">> ";
		getline(cin, command);
		if (command == "new") { // add new word
			cout << "Word: ";
			getline(cin, userInp);
			words.push_back(userInp);
			cout << "Trans: ";
			getline(cin, userInp);
			trans.push_back(userInp);
		} else if (command == "list") { // show all words
			for (int i = 0; i < words.size(); ++i) {
				cout << words.at(i) << " - " << trans.at(i) << endl;
			}
			if (un_word.size() > 0 && un_trans.size() > 0) {
				for (int i = 0; i < un_word.size(); ++i) {
				cout << un_word.at(i) << " - " << un_trans.at(i) << endl;
			}
		}
		} else if (command == "unknow") { // add unknow word
			cout << "Word: ";
			getline(cin, userInp);
			un_word.push_back(userInp);
		} else if (command == "unknow list") { // show all unknow words
			for (auto j : un_word) { cout << j << endl; }
		}

		// add translate of unknow word
		for (int i = 0; i < un_word.size(); ++i) {
			string str = "trans ";
			if (command == str + un_word.at(i)) {
				cout << "Trans: ";
				getline(cin, userInp);
				un_trans.push_back(userInp);
			}
		}
	}
	return 0;
}
