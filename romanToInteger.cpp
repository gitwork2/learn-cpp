#include <iostream>
using namespace std;

int convertRomnIntgr(string romnNum) 
{
	short romnNumLength = romnNum.length();

	char romnNumExp[] {'I', 'V', 'X', 'L', 'C', 'D', 'M'};
	int romnNumVal[]  {1, 5, 10, 50, 100, 500, 1000};
	short arrLength = 7;

	int intgrNum[romnNumLength];
	int res = 0;
	
	for (short i = 0; i < romnNumLength; ++i) {
		for (short k = 0; k < arrLength; ++k) {
			if (romnNum[i] == romnNumExp[k]) {
				intgrNum[i] = romnNumVal[k];
				cout << intgrNum[i] << endl;
				break;
			}
		}
	}

	/* FIXING */
	res = intgrNum[0];
	for (int i = 0; i < romnNumLength-1; ++i) {
		if (intgrNum[i] >= intgrNum[i+1]) {
			res += intgrNum[i+1];
			cout << "+" << intgrNum[i+1] << endl;
		}
		if (intgrNum[i] < intgrNum[i+1]) {
			res += intgrNum[i+1] - intgrNum[i];
			cout << "-+" << intgrNum[i+1] - intgrNum[i] << endl;
		}
	}
	cout << "Result: " << res << endl;
	
	return res;
}
int main() 
{
	/* SETUP */
	cout << "Roman: ";
	string romnNum;
	getline(cin>>ws, romnNum);
	int res = convertRomnIntgr(romnNum);

	cout << res << endl;
	return 0;
}
